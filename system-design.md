# Interview homework

this is prep for the systems design part of my interview loop, this doesn't represent anything real. These are my notes and should be treated as such and not a design doc.

## Assumptions

* Assume the IaaS / PaaS layer is AWS and we can use their services
  ** Assuming we use object storage for the actual files, I'd rather stick to S3 constructs and not be delving into CEPH architecture
  ** Same with key-value store, assume DynamoDB and that we are using Dynamo global tables for multi-master replication.
  ** We're going to run this in 2 regions
  ** Given we will need to rely on IAM policies for certain things, we'll assume they are appropriately managed and that IAM policy changes are recorded elsewhere.
  ** We're going to use 2 AWS accounts, one for the main system, another for the archive storage
  ** We'll leverage a standard AWS APi Gateway with Cognito to use oauth with auth-n and auth-z
* The mechanism / service called for upload is important, not the device that uploads it.
* When the file is uploaded there will be certain meta-data we need to record
* A file is a file, regardless of wether it's video or something like a document that's manually uploaded through a UI
* Need to maintain evidence integrity, we need to be able to detect if a file has been modified.
* This is really a classic document management problem with additional compliance angles to cover.
* UUID's for people interacting with the system will be formatted <agencyid>-<personid>

## Application Security model

We need to assume this will be compromised at some point and put in a defensive architecture, one that allows us to maintain the integrity of the actual evidence above all else.

Given we are going to rely on an IAM role to restrict deletes and updates, we also need to make sure we archive the data to another AWS account. The secondary AWS account can be severely locked down, very limited access / no access with a 'break glass moment'

This should mean that if we are compromised because of an application or end point issue, the original evidence and it's meta-data are safe.

In addition, in the primary AWS account, we can ensure that the AWS API is only accessible from the company's locations, leveraging strong multifactor and RO accounts - no one should need write access to this account once it's created.

IAM roles should be created and updated through automation, from a system secured in a 3rd AWS account. Updates to the IAM roles in the primary account should ONLY permitted to be written by the root account (break glass) or the automation in the 3rd account.

We'll need to attach all of the usual security measures, WAF, etc to protect the API as much as possible.

For run time environment, let's leverage a walled garden like Fargate; this should give us an additional level of isolation, it will limit what we can do with things like monitoring, but I think the tradeoff is worth it to remove another potential attack surface should we be compromised.

This also means there's no local accounts.

For the containers themselves, we can leverage a pod security policy in fargate, applying fairly standard protections. Pods themselves will have no credentials in them, instead access to resources will be given by a Pod execution policy.

With regards to auth-n and auth-z for the API, we'll need to integrate API gatewith with cognito for O-auth support, ultimately, this will need to be authenticated at whatever front end exists and a JWT will need to be passed.

## Upload Datastore elements

We will need to record certain meta-data at upload time, this will include the location the file is stored in and certain key pieces of data to ensure integrity. we will essentially use this an index to search for relevant files. This will need to be a key-value store that you can write to, but not update or delete rows from.

We'll need to leverage an IAM policy for that: https://github.com/aws-samples/aws-dynamodb-examples/blob/master/DynamoDBIAMPolicies/AmazonDynamoDBAppendOnlyAccess.json

We'll also use this to generate / store the UUID of the evidence and use this as a primary key across the other stores to tie this together.

We could potentially shard this by agency if needed for scalability and speed.

* UUID of this piece of evidence.
* ID of the evidence gatherer
* ID of the evidence uploader
* Agency CaseID
* datetime of submission
* datetime evidence gathered
* lat-long of the location the evidence was gathered
* Type of evidence
  ** video
  ** document
  ** picture
* Location of file
* Checksum of the file.

## Eventlog Datastore elements

We know what the attributes that define the evidence that's uploaded, now we need to record what happens with it, including it's eventual deletion; whilst the actual evidence may be deleted, these datastores will still contain their records, this ensures if something is deleted / tampered with we still an audit trail.

Again, WORM style DynamoDB, restricted by IAM policy.

* UUID of the evidence file
* ID of the person responsible for the event.
* datetime of the event
* type of event:
  ** Upload
  ** Access
  ** Delete

## Evidence Storage

Let's use S3 for storage because we are dealing with billions of files of various sizes, we'll use circular replication to ensure items are synced between regions. With replication enabled, versioning will also have been enabled, this means any changes to the file are durably stored, replicated and recorded. Doing this will make the amount of storage used increase significantly.

We'll also leverage an AWS glacier bucket in each region, this will receive one-way replication from the main bucket, this will keep the 'cold storage' of evidence completely separate, this bucket is not accessible to anything other than the delete API.

## Traffic delivery

* We'll run this in 2 regions, with replicated data stores which region people hit doesn't matter that much
* We can leverage a standard CDN with WAF to offer some level of protection to the API
* If we stick with AWS constructs, Cloudfront doesn't support multi-region that natively, we'll need to use a Route53 DNS entry as the origin.
* Route53 healthchecks are slow and don't work well, so we'll write a monitoring function to monitor each region for the following:
  ** Errors from the Loadbalancer as detected (somewhat slowly) by cloudwatch
  ** regular health check monitoring
  ** Any front end RUM
* This agent will take certain regions out of the zone file upon failure.

## Deletion of Evidence

* Deletion should only happen upon request at the main API, when the delete call is submitted through the API we should obviously check the JWT assertion that they are permitted to do that.
* When the file is deleted we should record in the event log that we were asked to delete the file and then delete the file in the primary account's S3 bucket, cleaning up the tombstones and other replication markers.

Obviously, this doesn't delete evidence in the archive nor does it clear the eventlog.

To clear up the archive we would need a batch process to come around afterwards to remove the file, this should be on a duration consistent with statute of limitations or some other similar legal mechanism.

## Build out of environment

This is a reasonably simple architecture, it should be easy to maintain the base PaaS and IaaS elements in TF or similar, it should be executed from some other account anyway given it's elevated access.

Deployment to fargate is simply deploying a container via the API.

## Monitoring

Let's start by looking for the four golden signals:

* Errors - easy to get from Cloudwatch at the Loadbalancer
* Latency - again easy to get from Cloudwatch at the loadbalancer
  ** This can be pretty tricky to set thresholds within AWS, because not all requests are equal, some we want to take 2 seconds, others (like upload) could take an unspecified amount of time.
  ** We might have to split the methods out across two LB's to do this natively
* Saturation
  ** With the use of fargate we don't need to worry about the utilization of the hosts,
  ** containers should be scaling out with the HPA but we should measure their utilization regardless, in case we aren't able to scale out / fast enough.
* Traffic - this we can monitor at the CDN level, again using cloudwatch metrics.

We should be leveraging container insights, this will allow us to watch pod utilization for the golden signals and also alert us to how many replicas of each pod we are running, we would need to alert on:

* too few
* too many
* scale down too fast
* scale up too fast
